package ru.vk.kuts.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import ru.vk.kuts.entities.Group;
import ru.vk.kuts.entities.User;

@Repository
public interface GroupRepository extends ReactiveMongoRepository<Group, String> {
}
