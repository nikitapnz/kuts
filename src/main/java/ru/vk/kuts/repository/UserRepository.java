package ru.vk.kuts.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.User;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, String> {
    Mono<User> findFirstByLoginAndPassword(String login, String password);
    Mono<User> findFirstByLogin(String login);
    Flux<User> findAllByGroup(String group);
    Mono<Void> deleteByGroupId(String groupId);
}
