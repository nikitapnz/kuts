package ru.vk.kuts.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.Content;
import ru.vk.kuts.entities.Node;

@Repository
public interface ContentRepository extends ReactiveMongoRepository<Content, String> {
    Flux<Content> findAllByContent(String fileId);

    Mono<Content> findByNode(Node node);

    Mono<Content> findByNodeId(String nodeId);
}
