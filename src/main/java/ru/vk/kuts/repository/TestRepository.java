package ru.vk.kuts.repository;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.Node;
import ru.vk.kuts.entities.Test;

@Repository
public interface TestRepository extends ReactiveMongoRepository<Test, String> {
    Mono<Test> findByNode(Node node);

    Mono<Test> findByNodeId(String id);

    @Query(fields = "{\"questions.choices.correct\": 0}", value = "{\"node.id\": ?0}")
    Mono<Test> findByNodeIdExcludeCorrect(String id);
}
