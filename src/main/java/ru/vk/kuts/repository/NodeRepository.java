package ru.vk.kuts.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.Node;

@Repository
public interface NodeRepository extends ReactiveMongoRepository<Node, String> {
    Mono<Node> getFirstByParentNode(String parentId);
}
