package ru.vk.kuts.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.Test;
import ru.vk.kuts.entities.TestResult;
import ru.vk.kuts.entities.User;

@Repository
public interface TestResultRepository extends ReactiveMongoRepository<TestResult, String> {
    Flux<TestResult> findAllByUserOrderByCreateDateDesc(String userId);
    Mono<Void> deleteByUser(User user);
}
