package ru.vk.kuts.service;

import org.springframework.core.io.Resource;
import org.springframework.http.codec.multipart.FilePart;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface FileService {
    Mono<UUID> saveFile(FilePart filePart);
    Mono<Resource> getFile(String uuidFile);
    Mono<Void> deleteFile(String uuidFile);
}
