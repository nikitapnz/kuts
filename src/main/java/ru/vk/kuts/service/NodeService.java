package ru.vk.kuts.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.Node;

public interface NodeService {
    Mono<Node> save(Node node);

    Flux<Node> getAll();

    Mono<Node> getById(String id);

    Mono<Void> removeNode(String id);

    Mono<Node> getFirstByParentId(String parentId);
}
