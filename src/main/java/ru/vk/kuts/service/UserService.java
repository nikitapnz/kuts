package ru.vk.kuts.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.config.annotation.UserAuth;
import ru.vk.kuts.dto.TokenDTO;
import ru.vk.kuts.dto.UserAuthDTO;
import ru.vk.kuts.entities.User;
import ru.vk.kuts.vo.Role;

public interface UserService {
    Mono<User> createUser(User user, Role role);
    Mono<User> getUserByUUID(String userId);
    Mono<TokenDTO> generateToken(UserAuthDTO userAuthDTO);
    Flux<User> findUsersByGroupId(String groupId);
    Mono<Void> removeByGroupId(String groupId);
}
