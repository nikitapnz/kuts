package ru.vk.kuts.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.io.File;
import java.io.FileInputStream;
import java.util.UUID;

@Service
public class FileServiceImpl implements FileService {
    private final String filesDir;

    public FileServiceImpl(@Value("${kuts.file.path}") String filesDir) {
        this.filesDir = filesDir;
    }

    @Override
    public Mono<UUID> saveFile(FilePart filePart) {
        File directory = new File(filesDir);
        if (!directory.exists() && !directory.mkdir()){
            throw new IllegalStateException("Ошибка создания дерриктории для файлов");
        }

        UUID uuidFile = UUID.randomUUID();
        File uploadedFile = new File(filesDir + "/" + uuidFile);

        return filePart
                .transferTo(uploadedFile)
                .log()
                .then(Mono.just(uuidFile));
    }

    @Override
    public Mono<Resource> getFile(String uuidFile) {
        return Mono.just(new FileSystemResource(filesDir + "/" + uuidFile));
    }

    @Override
    public Mono<Void> deleteFile(String uuidFile) {
        return Mono.just(new File(filesDir + "/" + uuidFile).delete()).then(Mono.empty());
    }
}
