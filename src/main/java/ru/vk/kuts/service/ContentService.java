package ru.vk.kuts.service;

import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.Content;
import ru.vk.kuts.entities.Node;

public interface ContentService {
    Mono<Content> save(Content content);
    Mono<Content> getById(String id);
    Mono<Void> remove(Content content);
    Mono<Void> remove(String contentId);
    Mono<Void> removeFileFromContent(String fileId);
    Mono<Content> getContentByNode(Node node);
    Mono<Content> getContentByNodeId(String nodeId);
}
