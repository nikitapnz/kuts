package ru.vk.kuts.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.Content;
import ru.vk.kuts.entities.Node;
import ru.vk.kuts.repository.ContentRepository;

@Service
@RequiredArgsConstructor
public class ContentServiceImpl implements ContentService {
    private final ContentRepository contentRepository;
    private final FileService fileService;

    @Override
    public Mono<Content> save(Content content) {
        return contentRepository.save(content);
    }

    @Override
    public Mono<Content> getById(String id) {
        return contentRepository.findById(id);
    }

    @Override
    public Mono<Void> remove(Content content) {
        return contentRepository.delete(content)
                .then(fileService.deleteFile(content.getContent()));
    }

    @Override
    public Mono<Void> remove(String contentId) {
        return contentRepository.findById(contentId)
                .flatMap(this::remove);
    }

    @Override
    public Mono<Void> removeFileFromContent(String fileId) {
         return contentRepository
                 .findAllByContent(fileId)
                 .flatMap(content -> {
                     content.setContent(null);
                     return save(content);
                 })
                 .then();
    }

    @Override
    public Mono<Content> getContentByNode(Node node) {
        return contentRepository.findByNode(node);
    }

    @Override
    public Mono<Content> getContentByNodeId(String nodeId) {
        return contentRepository.findByNodeId(nodeId);
    }
}
