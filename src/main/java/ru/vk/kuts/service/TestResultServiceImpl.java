package ru.vk.kuts.service;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.TestResult;
import ru.vk.kuts.entities.User;
import ru.vk.kuts.repository.TestResultRepository;

@Service
public class TestResultServiceImpl implements TestResultService {
    private final TestResultRepository testResultRepository;

    public TestResultServiceImpl(TestResultRepository testResultRepository) {
        this.testResultRepository = testResultRepository;
    }

    @Override
    public Mono<TestResult> save(TestResult testResult) {
        return testResultRepository.save(testResult);
    }

    @Override
    public Flux<TestResult> findAlltestResultsByUserId(String userId) {
        return testResultRepository.findAllByUserOrderByCreateDateDesc(userId);
    }

    @Override
    public Mono<Void> removeByUser(User user) {
        return testResultRepository.deleteByUser(user);
    }
}
