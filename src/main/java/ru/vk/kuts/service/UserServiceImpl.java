package ru.vk.kuts.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.config.annotation.AuthTokenMap;
import ru.vk.kuts.dto.TokenDTO;
import ru.vk.kuts.dto.UserAuthDTO;
import ru.vk.kuts.entities.User;
import ru.vk.kuts.repository.UserRepository;
import ru.vk.kuts.vo.Role;
import ru.vk.kuts.vo.TokenAuth;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final Map<String, TokenAuth> authTokenMap;
    private final TestResultService testResultService;

    // in minutes
    private final int tokenLifeTime;

    public UserServiceImpl(UserRepository userRepository,
                           @AuthTokenMap Map<String, TokenAuth> authTokenMap,
                           TestResultService testResultService,
                           @Value("${auth.token.lifetime}") int tokenLifeTime) {
        this.userRepository = userRepository;
        this.authTokenMap = authTokenMap;
        this.testResultService = testResultService;
        this.tokenLifeTime = tokenLifeTime;
    }

    @Override
    public Mono<User> createUser(User user, Role role) {
        return userRepository.findFirstByLogin(user.getLogin())
                .map(e -> {
                    if (e != null) {
                        throw new IllegalStateException("Пользователь с таким логином уже существует");
                    }
                    return e;
                })
                .switchIfEmpty(Mono.defer(() -> {
                    user.setRole(role);
                    return userRepository.save(user);
                }));
    }

    @Override
    public Mono<User> getUserByUUID(String userId) {
        return userRepository.findById(userId);
    }

    @Override
    public Mono<TokenDTO> generateToken(UserAuthDTO userAuthDTO) {
        return userRepository.findFirstByLoginAndPassword(userAuthDTO.getLogin(), userAuthDTO.getPassword())
                .flatMap(user -> {
                    String authToken = UUID.randomUUID().toString();
                    authTokenMap.put(
                            authToken,
                            TokenAuth.builder()
                                    .token(authToken)
                                    .expiredDate(LocalDateTime.now().plusMinutes(tokenLifeTime))
                                    .user(user)
                                    .build()
                    );
                    return Mono.just(new TokenDTO(authToken, user.getRole()));
                });
    }

    @Override
    public Flux<User> findUsersByGroupId(String groupId) {
        return userRepository.findAllByGroup(groupId);
    }

    @Override
    public Mono<Void> removeByGroupId(String groupId) {
        return userRepository.findAllByGroup(groupId)
                .flatMap(e -> testResultService.removeByUser(e).then(userRepository.delete(e)))
                .then();
    }
}
