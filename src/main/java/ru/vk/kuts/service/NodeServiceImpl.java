package ru.vk.kuts.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.Node;
import ru.vk.kuts.repository.NodeRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class NodeServiceImpl implements NodeService {
    private final NodeRepository nodeRepository;
    private final ContentService contentService;
    private final TestService testService;

    @Override
    public Mono<Node> save(Node node) {
       return nodeRepository.save(node);
    }

    @Override
    public Flux<Node> getAll() {
        return nodeRepository
                .findAll()
                .collectList()
                .flatMap(list -> Mono.just(getRootNodes(list)))
                .flatMapMany(Flux::fromIterable);
    }

    @Override
    public Mono<Node> getById(String id) {
        return nodeRepository.findById(id);
    }

    @Override
    public Mono<Void> removeNode(String id) {
        return nodeRepository.findById(id)
                .flatMap(node -> getFirstByParentId(id)
                                .flatMap(n -> Mono.error(new IllegalStateException("Есть зависимые элементы")))
                                .then(contentService
                                        .getContentByNode(node)
                                        .flatMap(contentService::remove)
                                .then(testService
                                        .getTestByNode(node)
                                        .flatMap(testService::remove))
                                .then(nodeRepository.delete(node))));
    }

    @Override
    public Mono<Node> getFirstByParentId(String parentId) {
        return nodeRepository.getFirstByParentNode(parentId);
    }

    private List<Node> getRootNodes(List<Node> nodes) {
        List<Node> root = new ArrayList<>();
        nodes.stream()
                .filter(el -> el.getParentNode() == null)
                .forEach(el -> {
                    addChildrenToNode(nodes, el);
                    root.add(el);
                });
        return root;
    }

    private void addChildrenToNode(List<Node> list, Node root) {
        List<Node> children;
        for (Node node: list) {
            if (Objects.equals(node.getParentNode(), root.getId())) {
                addChildrenToNode(list, node);
                children = root.getChildren() == null
                        ? new ArrayList<>()
                        : root.getChildren();
                children.add(node);
                root.setChildren(children);
            }
        }
    }
}
