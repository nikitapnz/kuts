package ru.vk.kuts.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.Group;

public interface GroupService {

    Mono<Group> createGroup(Group group);

    Flux<Group> getAllGroups();

    Mono<Group> findById(String id);

    Mono<Void> deleteById(String id);

}
