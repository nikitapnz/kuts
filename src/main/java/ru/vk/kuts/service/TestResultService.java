package ru.vk.kuts.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.TestResult;
import ru.vk.kuts.entities.User;

public interface TestResultService {
    Mono<TestResult> save(TestResult testResult);
    Flux<TestResult> findAlltestResultsByUserId(String userId);
    Mono<Void> removeByUser(User user);
}
