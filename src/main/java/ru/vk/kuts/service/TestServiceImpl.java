package ru.vk.kuts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.vk.kuts.dto.ResultTestDTO;
import ru.vk.kuts.entities.*;
import ru.vk.kuts.repository.TestRepository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TestServiceImpl implements TestService {
    private final TestRepository testRepository;
    private final TestResultService testResultService;

    public TestServiceImpl(TestRepository testRepository, TestResultService testResultService) {
        this.testRepository = testRepository;
        this.testResultService = testResultService;
    }

    @Override
    public Mono<Test> save(Test test) {
        return Mono.fromRunnable(() -> {
            if ((test.getQuestions() != null) && (!test.getQuestions().isEmpty())) {
                test.getQuestions().forEach(q -> {
                    q.setId(UUID.randomUUID().toString());
                    if ((q.getChoices() != null) && (!q.getChoices().isEmpty())) {
                        q.getChoices().forEach(c -> c.setId(UUID.randomUUID().toString()));
                    }
                });
            } else {
                throw new IllegalStateException("Test incorrect");
            }
        })
        .then(testRepository.save(test));
    }

    @Override
    public Mono<Test> getById(String id) {
        return testRepository.findById(id);
    }

    @Override
    public Mono<Void> remove(Test test) {
        return testRepository.delete(test);
    }

    @Override
    public Mono<Test> getTestByNode(Node node) {
        return testRepository.findByNode(node);
    }

    @Override
    public Mono<Test> getTestByNodeId(String nodeId) {
        return testRepository.findByNodeId(nodeId);
    }

    @Override
    public Mono<Test> getTestByNodeIdExcludeCorrect(String nodeId) {
        return testRepository.findByNodeIdExcludeCorrect(nodeId);
    }

    @Override
    public Mono<ResultTestDTO> validateTest(Test test, User user) {
        return testRepository
                .findById(test.getId())
                .flatMap(testRep -> Mono.just(equalsTests(test, testRep)))
                .flatMap(resultTestDTO -> {
                    TestResult testResult = new TestResult();
                    testResult.setCreateDate(LocalDateTime.now().atZone(ZoneId.of("Europe/Moscow")).toInstant().toEpochMilli());
                    testResult.setResultTest(resultTestDTO.getResultTest());
                    testResult.setTest(test);
                    testResult.setTestName(test.getTestName());
                    testResult.setUser(user);
                    return testResultService.save(testResult)
                           .then(Mono.just(resultTestDTO));
                });
    }

    private ResultTestDTO equalsTests(Test answer, Test testRep) {
        ResultTestDTO result = new ResultTestDTO(answer.getId(), new HashMap<>());

        List<Question> questionRep = testRep.getQuestions();
        List<Question> questionAnswer = answer.getQuestions();
        if ((questionRep.size() == 0) && (questionAnswer.size() == 0)) {
            return result; // Сразу правильно за весь тест
        }

        questionRep.forEach(qRep -> {
            Question ans = questionAnswer.stream().filter(qAns -> qAns.getId().equals(qRep.getId())).findFirst().orElse(null);
            List<Choice> choiceRep = qRep.getChoices() == null
                    ? Collections.emptyList()
                    : qRep.getChoices().stream().filter(e -> e.getCorrect() != null && e.getCorrect()).collect(Collectors.toList());

            List<Choice> choiceAns = ((ans == null) || (ans.getChoices() == null))
                    ? Collections.emptyList()
                    : ans.getChoices().stream().filter(e -> e.getCorrect() != null && e.getCorrect()).collect(Collectors.toList());

            if (choiceRep.size() != choiceAns.size()) {
                result.getResultTest().put(qRep.getId(), false);
                return;
            }

            List<Choice> isPresentInAnswer = choiceRep.stream()
                    .filter(chRep -> choiceAns.stream().anyMatch(chAns -> chAns.getId().equals(chRep.getId())))
                    .collect(Collectors.toList());

            if (choiceRep.size() != isPresentInAnswer.size()) {
                result.getResultTest().put(qRep.getId(), false);
            } else {
                result.getResultTest().put(qRep.getId(), true);
            }
        });

        return result;
    }
}
