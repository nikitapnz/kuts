package ru.vk.kuts.service;

import reactor.core.publisher.Mono;
import ru.vk.kuts.dto.ResultTestDTO;
import ru.vk.kuts.entities.Node;
import ru.vk.kuts.entities.Test;
import ru.vk.kuts.entities.User;

public interface TestService {
    Mono<Test> save(Test test);
    Mono<Test> getById(String id);
    Mono<Void> remove(Test test);
    Mono<Test> getTestByNode(Node node);
    Mono<Test> getTestByNodeId(String nodeId);
    Mono<Test> getTestByNodeIdExcludeCorrect(String nodeId);
    Mono<ResultTestDTO> validateTest(Test test, User user);
}
