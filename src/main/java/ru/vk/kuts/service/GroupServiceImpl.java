package ru.vk.kuts.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.Group;
import ru.vk.kuts.repository.GroupRepository;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {
    private final GroupRepository groupRepository;
    private final UserService userService;

    public Mono<Group> createGroup(Group group) {
        return groupRepository.save(group);
    }

    public Flux<Group> getAllGroups() {
        return groupRepository.findAll();
    }

    public Mono<Group> findById(String id) {
        return groupRepository.findById(id);
    }

    @Override
    public Mono<Void> deleteById(String id) {
        return groupRepository.deleteById(id)
                .then(userService.removeByGroupId(id));
    }
}
