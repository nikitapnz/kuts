package ru.vk.kuts.filter;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;
import ru.vk.kuts.config.annotation.AuthTokenMap;
import ru.vk.kuts.config.annotation.UserAuth;
import ru.vk.kuts.vo.Role;
import ru.vk.kuts.vo.TokenAuth;

import java.lang.annotation.Annotation;
import java.time.LocalDateTime;
import java.util.Map;

@Component
public class UserAuthFilter implements WebFilter {
    private static String AUTH_HEADER = "Authorization";

    private final RequestMappingHandlerMapping requestMappingHandlerMapping;
    private final Map<String, TokenAuth> tokenAuthMap;


    public UserAuthFilter(RequestMappingHandlerMapping requestMappingHandlerMapping,
                          @AuthTokenMap Map<String, TokenAuth> tokenAuthMap) {
        this.requestMappingHandlerMapping = requestMappingHandlerMapping;
        this.tokenAuthMap = tokenAuthMap;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange serverWebExchange, WebFilterChain webFilterChain) {
        Object object = requestMappingHandlerMapping
                .getHandler(serverWebExchange)
                .toProcessor().peek();

        if (!(object instanceof HandlerMethod)) {
            return webFilterChain.filter(serverWebExchange);
        }
        HandlerMethod handler = (HandlerMethod) object;

        UserAuth userAuthAnnotation = handler.getMethodAnnotation(UserAuth.class);
        if (userAuthAnnotation == null) { return webFilterChain.filter(serverWebExchange); }

        String authToken = getAuthToken(serverWebExchange);

        TokenAuth tokenAuth = tokenAuthMap.get(authToken);
        if (tokenAuth == null || tokenAuth.getExpiredDate().isBefore(LocalDateTime.now())) {
            throw new HttpClientErrorException(HttpStatus.FORBIDDEN);
        }

        if (!tokenAuth.getUser().getRole().equals(Role.ADMIN)
                && !tokenAuth.getUser().getRole().equals(userAuthAnnotation.value())) {
            throw new HttpClientErrorException(HttpStatus.FORBIDDEN);
        }

        serverWebExchange.getAttributes().put("user", tokenAuth.getUser());
        return webFilterChain.filter(serverWebExchange);
    }

    private String getAuthToken(ServerWebExchange serverWebExchange) {
        try {
            return serverWebExchange.getRequest().getHeaders().get(AUTH_HEADER).get(0);
        } catch (Exception ex) {
            throw new HttpClientErrorException(HttpStatus.FORBIDDEN);
        }
    }
}
