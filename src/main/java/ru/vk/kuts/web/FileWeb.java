package ru.vk.kuts.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ru.vk.kuts.config.annotation.UserAuth;
import ru.vk.kuts.service.ContentService;
import ru.vk.kuts.service.FileService;
import ru.vk.kuts.vo.Role;

import java.util.UUID;

@RestController
public class FileWeb {
    private final FileService fileService;

    private final ContentService contentService;

    public FileWeb(FileService fileService, ContentService contentService) {
        this.fileService = fileService;
        this.contentService = contentService;
    }

    @RequestMapping(value = "/api/1/file",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            method = RequestMethod.POST)
    public Mono<UUID> saveFile(@RequestPart("file") Mono<FilePart> part){
        return part.flatMap(fileService::saveFile);
    }

    @RequestMapping(value = "/api/1/file/{uuidFile}", method = RequestMethod.GET)
    public ResponseEntity<Mono<Resource>> getFile(@PathVariable String uuidFile) {
        return ResponseEntity.ok()
                .body(fileService.getFile(uuidFile));
    }

    @RequestMapping(value = "/api/1/file/{uuidFile}", method = RequestMethod.DELETE)
    public Mono<Void> deleteFile(@PathVariable String uuidFile) {
        return fileService
                .deleteFile(uuidFile)
                .then(contentService.removeFileFromContent(uuidFile));
    }
}
