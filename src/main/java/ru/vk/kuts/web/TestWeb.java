package ru.vk.kuts.web;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.config.annotation.UserAuth;
import ru.vk.kuts.dto.ResultTestDTO;
import ru.vk.kuts.entities.Node;
import ru.vk.kuts.entities.Test;
import ru.vk.kuts.entities.TestResult;
import ru.vk.kuts.service.NodeService;
import ru.vk.kuts.service.TestResultService;
import ru.vk.kuts.service.TestService;
import ru.vk.kuts.vo.Role;

@RestController
@RequiredArgsConstructor
public class TestWeb {
    private final TestService testService;
    private final TestResultService testResultService;
    private final NodeService nodeService;

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/test", method = RequestMethod.POST)
    public Mono<Test> saveTest(@RequestBody Test test) {
        return testService.save(test).flatMap(t -> {
            return nodeService.getById(t.getNode().getId())
                    .flatMap(node -> {
                        node.setHasTest(Boolean.TRUE);
                        return nodeService.save(node);
                    }).then(Mono.just(t));
        });
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/test/{nodeId}", method = RequestMethod.GET)
    public Mono<Test> getTestByNodeId(@PathVariable String nodeId) {
        return testService.getTestByNodeId(nodeId);
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/test/{nodeId}/start", method = RequestMethod.GET)
    public Mono<Test> getTestByNodeIdExcludeCorrect(@PathVariable String nodeId) {
        return testService.getTestByNodeIdExcludeCorrect(nodeId);
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/test/{nodeId}/validate", method = RequestMethod.POST)
    public Mono<ResultTestDTO> getTestByNodeIdExcludeCorrect(@RequestBody Test test, ServerWebExchange serverWebExchange) {
        return testService.validateTest(test, serverWebExchange.getAttribute("user"));
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/test/testResult/{userId}", method = RequestMethod.GET)
    public Flux<TestResult> getTestResults(@PathVariable String userId) {
        return testResultService.findAlltestResultsByUserId(userId);
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/test/{testId}", method = RequestMethod.DELETE)
    public Mono<Void> removeTest(@PathVariable String testId) {
        return testService.getById(testId).flatMap(t -> {
            return nodeService.getById(t.getNode().getId())
                    .flatMap(node -> {
                        node.setHasTest(Boolean.FALSE);
                        return nodeService.save(node);
                    }).then(testService.remove(t));
        });
    }

}
