package ru.vk.kuts.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.config.annotation.UserAuth;
import ru.vk.kuts.entities.Node;
import ru.vk.kuts.service.NodeService;
import ru.vk.kuts.vo.Role;

@RestController
public class NodeWeb {
    private final NodeService nodeService;

    public NodeWeb(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @RequestMapping(path = "/api/1/node", method = RequestMethod.POST)
    public Mono<Node> saveNode(@RequestBody Node node) {
        if (node.getHasContent() == null) {
            node.setHasContent(Boolean.FALSE);
        }

        if (node.getHasTest() == null) {
            node.setHasTest(Boolean.FALSE);
        }

        return nodeService.save(node);
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/node/{id}", method = RequestMethod.GET)
    public Mono<Node> getNodeById(@PathVariable String id) {
        return nodeService.getById(id);
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/node/findAll", method = RequestMethod.GET)
    public Flux<Node> getAllNodes() {
        return nodeService.getAll();
    }

    @RequestMapping(path = "/api/1/node/{id}", method = RequestMethod.DELETE)
    public Mono<Void> removeNode(@PathVariable String id) {
        return nodeService.removeNode(id);
    }

    @RequestMapping(path = "/api/1/node", method = RequestMethod.PUT)
    public Mono<Node> updateNode(@RequestBody Node node) {
        return nodeService.save(node);
    }
}
