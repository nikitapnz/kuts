package ru.vk.kuts.web;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.config.annotation.UserAuth;
import ru.vk.kuts.entities.Group;
import ru.vk.kuts.service.GroupService;
import ru.vk.kuts.vo.Role;

@RestController
@RequiredArgsConstructor
public class GroupWeb {
    private final GroupService groupService;

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/group", method = RequestMethod.POST)
    public Mono<Group> createGroup(@RequestBody Group group) {
        return groupService.createGroup(group);
    }

    @RequestMapping(path = "/api/1/group/list", method = RequestMethod.GET)
    public Flux<Group> getAllGroups() {
        return groupService.getAllGroups();
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/group/{groupId}", method = RequestMethod.GET)
    public Mono<Group> getGroupById(@PathVariable("groupId") String groupId) {
        return groupService.findById(groupId);
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/group/{groupId}", method = RequestMethod.DELETE)
    public Mono<Void> deleteGroupById(@PathVariable("groupId") String groupId) {
        return groupService.deleteById(groupId);
    }

}
