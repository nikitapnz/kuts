package ru.vk.kuts.web;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ru.vk.kuts.config.annotation.UserAuth;
import ru.vk.kuts.entities.Content;
import ru.vk.kuts.service.ContentService;
import ru.vk.kuts.service.NodeService;
import ru.vk.kuts.vo.Role;


@RestController
@RequiredArgsConstructor
public class ContentWeb {
    private final ContentService contentService;
    private final NodeService nodeService;

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/content", method = RequestMethod.POST)
    public Mono<Content> saveContent(@RequestBody Content content) {
        return contentService.save(content).flatMap(t -> {
            return nodeService.getById(t.getNode().getId())
                    .flatMap(node -> {
                        node.setHasContent(Boolean.TRUE);
                        return nodeService.save(node);
                    }).then(Mono.just(t));
        });
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/content/{nodeId}", method = RequestMethod.GET)
    public Mono<Content> getContentByNodeId(@PathVariable String nodeId) {
        return contentService.getContentByNodeId(nodeId);
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/content/{contentId}", method = RequestMethod.DELETE)
    public Mono<Void> removeContent(@PathVariable String contentId) {
        return contentService.getById(contentId).flatMap(t -> {
            return nodeService.getById(t.getNode().getId())
                    .flatMap(node -> {
                        node.setHasContent(Boolean.FALSE);
                        return nodeService.save(node);
                    }).then(contentService.remove(t));
        });
    }

}
