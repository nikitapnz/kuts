package ru.vk.kuts.web;

import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.vk.kuts.config.annotation.UserAuth;
import ru.vk.kuts.dto.TokenDTO;
import ru.vk.kuts.dto.UserAuthDTO;
import ru.vk.kuts.entities.User;
import ru.vk.kuts.service.UserService;
import ru.vk.kuts.vo.Role;

@RestController
public class UserWeb {
    private final UserService userService;

    public UserWeb(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(path = "/api/1/user/register", method = RequestMethod.POST)
    public Mono<TokenDTO> registerUser(@RequestBody User user) {
        return userService.createUser(user, Role.USER)
                .map(registred -> new UserAuthDTO(registred.getLogin(), registred.getPassword()))
                .flatMap(userService::generateToken)
                .switchIfEmpty(Mono.error(new IllegalStateException("Авторизация прошла неудачно")));
    }

    @RequestMapping(path = "/api/1/user/auth", method = RequestMethod.POST)
    public Mono<TokenDTO> authUser(@RequestBody UserAuthDTO userAuthDTO) {
        return userService.generateToken(userAuthDTO)
                .switchIfEmpty(Mono.error(new IllegalStateException("Авторизация прошла неудачно")));
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/user/group/{groupId}", method = RequestMethod.GET)
    public Flux<User> getUsersByGroupId(@PathVariable String groupId) {
        return userService.findUsersByGroupId(groupId);
    }

    //TODO need migration for mongodb
    @RequestMapping(path = "/adm", method = RequestMethod.GET)
    public Mono<TokenDTO> registerAdmin() {
        User user = new User();
        user.setLogin("admin");
        user.setPassword("admin");
        user.setName("admin");
        return userService.createUser(user, Role.ADMIN)
                .map(registred -> new UserAuthDTO(registred.getLogin(), registred.getPassword()))
                .flatMap(userService::generateToken)
                .switchIfEmpty(Mono.error(new IllegalStateException("Авторизация прошла неудачно")));
    }
}
