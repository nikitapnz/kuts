package ru.vk.kuts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import reactor.core.publisher.Mono;
import ru.vk.kuts.entities.User;
import ru.vk.kuts.repository.UserRepository;
import ru.vk.kuts.vo.Role;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

@EnableScheduling
@EnableAspectJAutoProxy
@SpringBootApplication
public class KutsApplication {

	public static void main(String[] args) {
		SpringApplication.run(KutsApplication.class, args);
	}

}
