package ru.vk.kuts.entities;

import com.mongodb.lang.NonNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.vk.kuts.vo.ContentType;

@Getter @Setter
@Document(collection = "content")
public class Content {

    @Id
    private String id;

    private String header;

    private ContentType contentType;

    private String content;

    @DBRef
    private Node node;
}
