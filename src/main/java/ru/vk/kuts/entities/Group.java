package ru.vk.kuts.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter @Setter
@Document(collection = "group")
public class Group {
    @Id
    private String id;
    private String name;

    @JsonIgnore
    @Transient
    private List<User> users;
}
