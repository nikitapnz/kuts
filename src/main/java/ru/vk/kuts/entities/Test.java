package ru.vk.kuts.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter @Setter
@Document(collection = "test")
public class Test {

    @Id
    private String id;

    private String testName;

    private List<Question> questions;

    @DBRef
    private Node node;
}
