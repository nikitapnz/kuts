package ru.vk.kuts.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.vk.kuts.vo.Role;

@Getter @Setter
@Document(collection = "user")
public class User {
    @Id
    private String id;

    private String login;
    private String password;

    private String name;

    private Role role;

    @DBRef
    private Group group;
}
