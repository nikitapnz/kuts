package ru.vk.kuts.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Choice {

    private String id;

    private String choiceName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean correct;
}
