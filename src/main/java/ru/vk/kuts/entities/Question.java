package ru.vk.kuts.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter @Setter
public class Question {
    private String id;

    private String questionName;

    private List<Choice> choices;
}
