
package ru.vk.kuts.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter @Setter
@Document(collection = "node")
public class Node {
    @Id
    private String id;

    private String fullName;

    private String parentNode;

    private Boolean hasContent;

    private Boolean hasTest;

    @Transient
    private List<Node> children;
}
