package ru.vk.kuts.entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Getter
@Setter
@Document(collection = "test_result")
public class TestResult {

    @Id
    private String id;

    private String testName;

    private Map<String, Boolean> resultTest;

    private Long createDate;

    @DBRef
    private Test test;

    @DBRef
    private User user;
}
