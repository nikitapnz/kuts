package ru.vk.kuts.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.vk.kuts.entities.User;

import java.time.LocalDateTime;

@Getter
@Builder
@RequiredArgsConstructor
public class TokenAuth {
    private final User user;
    private final LocalDateTime expiredDate;
    private final String token;
}
