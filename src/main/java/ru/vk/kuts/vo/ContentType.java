package ru.vk.kuts.vo;

public enum ContentType {
    TEXT("Текстовый документ"),
    PDF("PDF документ"),
    FLESH("Flesh приложение"),
    HTML("HTML документ"),
    MP4("MP4 видео");

    private String title;

    ContentType(String title) {
        this.title = title;
    }
}
