package ru.vk.kuts.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor
public class ResultTestDTO {
    private final String testId;
    private final Map<String, Boolean> resultTest;
}