package ru.vk.kuts.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.vk.kuts.vo.Role;

@Getter
@RequiredArgsConstructor
public class TokenDTO {
    private final String token;
    private final Role role;
}
