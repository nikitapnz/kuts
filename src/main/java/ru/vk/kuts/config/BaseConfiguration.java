package ru.vk.kuts.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.vk.kuts.config.annotation.AuthTokenMap;
import ru.vk.kuts.vo.TokenAuth;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class BaseConfiguration {

    @Bean
    @AuthTokenMap
    public Map<String, TokenAuth> authTokenMap() {
        return new ConcurrentHashMap<>();
    }

}
