package ru.vk.kuts.config;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.vk.kuts.config.annotation.AuthTokenMap;
import ru.vk.kuts.vo.TokenAuth;

import java.time.LocalDateTime;
import java.util.Map;

@Component
public class SchedulerInvalidateToken {
    private final Map<String, TokenAuth> tokenAuthMap;

    public SchedulerInvalidateToken(@AuthTokenMap Map<String, TokenAuth> tokenAuthMap) {
        this.tokenAuthMap = tokenAuthMap;
    }

    @Scheduled(fixedRate = 60_000)
    public void invalidateToken() {
        tokenAuthMap.entrySet()
                .removeIf(auth -> auth.getValue().getExpiredDate().isBefore(LocalDateTime.now()));
    }
}
